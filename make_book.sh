#!/usr/bin/env bash
./make_document.sh
pandoc \
    --latex-engine=xelatex \
    -V geometry:margin=1in \
    -f markdown+pandoc_title_block \
    README.md -s -o book.pdf

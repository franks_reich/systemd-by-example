cmake_minimum_required(VERSION 3.8)
project(systemd-by-example VERSION 0.0.1 LANGUAGES CXX)

add_subdirectory(examples)

add_subdirectory(example-sd-event-and-sd-bus)
add_subdirectory(example-service-dbus-activation)
add_subdirectory(example-sd-bus-client)
add_subdirectory(example-logging-in-systemd)
add_subdirectory(example-journal-message-catalog)

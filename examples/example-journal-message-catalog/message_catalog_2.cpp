#include <iostream>

#include <systemd/sd-journal.h>
#include <systemd/sd-id128.h>


#define MESSAGE_WITH_VARIABLES SD_ID128_MAKE(dd,c8,5d,3a,25,20,4e,41,a3,8f,b1,d9,b4,ce,2a,8f)

using namespace std;

int main(int argc, char *argv[]) {
    char messageId[33];
    sd_id128_to_string(MESSAGE_WITH_VARIABLES, messageId);
    sd_journal_send(
            "MESSAGE_ID=%s", messageId,
            "MESSAGE=Message with the example message id",
            "PRIORITY=%i", LOG_ERR,
            nullptr);
}

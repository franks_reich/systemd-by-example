# Automatically Starting a Service using D-Bus Activation

**Note:** The files for this example can be found in the GitLab repository for the book
[^book-repository] in the directory **examples/example-service-dbus-activation**.

Similar to socket activation systemd allows activating applications on demand using the
D-Bus bus. Activating a service using D-Bus is strictly speaking a feature of D-Bus. To know
which service is provides by which executable D-Bus uses service files in the directory
**/usr/share/dbus-1/services** or **/usr/share/dbus-1/system-services**. The former is used
for system services, the latter for session services. To connect the D-Bus service file
with a systemd service unit, the configuration file provides the *SystemdService* key which
can be used to provide the name of a systemd unit that will be started instead of the
normally supplied executable. Because systemd unit have to provide the user name that is
used to run the executable and services on the session bus run under the user name to whom
the session bus belongs, using systemd services in D-Bus service files is only meaningful
for system services.

Therefore I will change the application we used from the prior example so that it connects
to the system bus instead of the session bus by changing the call to `sd_bus_default_user()`
to `sd_bus_default_system()` to connect to the system bus instead.

The name of the D-Bus service file can be chosen arbitrarily, the D-Bus daemon will scan
all the files in the respective directories for a matching *BusName* option when it
encounters a call to a service that was not started beforehand. I use following D-Bus
service file to configure starting the test service.

!INCLUDECODE "examples/example-service-dbus-activation/d-bus-activation-example.d-bus.service"

The D-Bus service file describes the service to be started. It specifies that it provides a 
service named **net.franksreich.counter1** specified in the *Name* option. The *Exec*
option specifies the name of the executable and the *User* option specifies that the
service should be run by the user root. The only systemd related option *SystemdService*
specifies the name of the unit that should be started instead of starting the executable
directly.

I use the following systemd unit file to start the systemd service.

!INCLUDECODE "examples/example-service-dbus-activation/d-bus-activation-example.service"

The systemd unit file states that the type of the service is D-Bus in the *Type* option.
It mentions the name of the service as **net.franksreich.counter1**, the same name as in
the D-Bus service file. The executable is the same executable mentioned in the D-Bus
service file, too.

To test the example I copy the D-Bus service file **d-bus-activation-example.d-bus.service**
to **/usr/share/dbus-1/system-services** and **d-bus-activation-example.service** to
**/etc/systemd/system**. Additionally I copy the executable **d-bus-activation-example** to
the directory **/usr/bin/**.

The service unit does not include an *Install* section and therefore does not have to be
enabled. The D-Bus daemon automatically scans the services files in the configuration
directory. Calling `busctl` lists the service I registered as activatable.

```
...
fi.epitest.hostap.WPASupplicant            883 wpa_supplicant  root             :1.10         wpa_supplicant.service    -          -
fi.w1.wpa_supplicant1                      883 wpa_supplicant  root             :1.10         wpa_supplicant.service    -          -
net.franksreich.counter1                     - -               -                (activatable) -                         -
net.hadess.SensorProxy                     850 iio-sensor-prox root             :1.3          iio-sensor-proxy.service  -          -
org.bluez                                  848 bluetoothd      root             :1.5          bluetooth.service         -          -
...
```

Trying to start the service using
`busctl introspect net.franksreich.counter1 /net/franksreich/counter1` or
`sudo busctl introspect net.franksreich.counter1 /net/franksreich/counter1` yields the
following error.

```
Failed to introspect object /net/franksreich/counter1 of service net.franksreich.counter1: Access denied
```

This happens because the service does not have a policy definition so D-Bus prevents
everybody to call the service as default setting. Policy definitions for system bus services
are located in **/etc/dbus-1/system.d** and policy definitions for session bus services are
located in **/etc/dbus-1/session.d**. I create the config file
**net.franksreich.counter1.conf** in directory **/etc/dbus-1/system.d** with the following
content.

!INCLUDECODE "examples/example-service-dbus-activation/net.franksreich.counter1.conf"

The configuration file defines a *<busconfig>* node with two *<policy>* nodes. The first
*<policy>* node specifies that user *root* is allowed to tkae the name
**net.franksreich.counter1** name. The second *<policy>* node specifies that by default,
as indicated with the attribute *context="default"*, everybody can send and receive
messages to and from the service. The configuration file uses an xml language defined in
the D-Bus daemon man page [^man-dbus-daemon].

Calling `busctl introspect net.franksreich.counter1 /net/franksreich/counter1` now yields
the following.

```
NAME                                TYPE      SIGNATURE  RESULT/VALUE  FLAGS
net.franksreich.Manager             interface -          -             -
.AddCounter                         method    u          u             -
org.freedesktop.DBus.Introspectable interface -          -             -
.Introspect                         method    -          s             -
org.freedesktop.DBus.ObjectManager  interface -          -             -
.GetManagedObjects                  method    -          a{oa{sa{sv}}} -
.InterfacesAdded                    signal    oa{sa{sv}} -             -
.InterfacesRemoved                  signal    oas        -             -
org.freedesktop.DBus.Peer           interface -          -             -
.GetMachineId                       method    -          s             -
.Ping                               method    -          -             -
org.freedesktop.DBus.Properties     interface -          -             -
.Get                                method    ss         v             -
.GetAll                             method    s          a{sv}         -
.Set                                method    ssv        -             -
.PropertiesChanged                  signal    sa{sv}as   -             -
```

Calling `busctl` shows that the service has been activated.

```
...
com.ubuntu.WhoopsiePreferences               - -               -                (activatable) -                         -         
fi.epitest.hostap.WPASupplicant            883 wpa_supplicant  root             :1.10         wpa_supplicant.service    -          -        
fi.w1.wpa_supplicant1                      883 wpa_supplicant  root             :1.10         wpa_supplicant.service    -          -        
net.franksreich.counter1                  1435 d-bus-activatio root             :1.30257      d-bus-activation-examp…ce -          -        
net.hadess.SensorProxy                     850 iio-sensor-prox root             :1.3          iio-sensor-proxy.service  -          -        
org.bluez                                  848 bluetoothd      root             :1.5          bluetooth.service         -          -        
...
```

Calling `systemctl status d-bus-activation-example.service` also shows that the service
is active.

```
● d-bus-activation-example.service - Frank's Reich Counter Service
   Loaded: loaded (/etc/systemd/system/d-bus-activation-example.service; static; vendor preset: enabled)
   Active: active (running) since Mon 2018-10-08 19:17:47 EDT; 4min 59s ago
 Main PID: 1435 (d-bus-activatio)
    Tasks: 1 (limit: 4915)
   CGroup: /system.slice/d-bus-activation-example.service
           └─1435 /usr/bin/d-bus-activation-example

Oct 08 19:17:47 wotan systemd[1]: Starting Frank's Reich Counter Service...
Oct 08 19:17:47 wotan systemd[1]: Started Frank's Reich Counter Service.
```

D-Bus now starts the service using the systemd unit file which allows the service to
provide its interface to its clients and allows systemd to manage the service.

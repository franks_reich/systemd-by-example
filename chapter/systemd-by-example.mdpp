% Systemd by Example
% Frank Krick
% 10/8/2018


# Table of Contents

!TOC

Whether you like systemd or not, systemd is probably here to stay given that is used as
PID 1 in most unix distributions. Systemd was the subject of a lot of controversy in the
unix world, but that is not something I want to make the subject of my writing here.
Instead, I would say that whether you like systemd or not, it is useful to understand
systemd and to learn how to do things the systemd way.

In the spirit of full disclosure, let me point out my own biases here. I started out with
unix distributions using SysV init and later moved on to upstart based linux distribution.
As I was always comfortable with SysV init based distribution, given that the whole approach
is simple and easy to understand as most of the boot is managed by scripts, I was not
necessarily taken by storm when I finally moved to a linux distribution based on systemd.
On the other hand, the deeper I dug in to systemd, the more I could see the potential
applications which were hard to implement with other approaches, especially given the
interfaces that systemd provides for use by all kinds of programming languages and scripts
in a much more usable manner than calling shell scripts and parsing the output. 

As I could not find a lot of introductory as well as practical information regarding systemd,
I decided to write my own introductory and practical material based on my needs for several
projects of mine as well as to cure my own curiosity. Because I allege that there is no good
introductory material available, I will start this text with an overview of the relevant
material that I used as reference and basis for this text in the hope that the relevant
resources will be helpful not only to me but also to the reader.

The first and biggest source of information for systemd is the systemd wiki on
freedesktop.org, the official systemd wiki [^systemd-wiki]. The systemd wiki includes links
to several articles and blog posts for systemd functionality as well as the github repository
for the source code but does not include an in-depth introduction to systemd itself.

The systemd wiki offers links to articles about systemd in several publications which provide
high level introductions of systemd including the underlying philosophy in several languages.
These articles will be mentioned in the following sections if they are relevant for their
contents will be referenced there. Therefore I will not mention the respective articles here.
Additionally, the systemd wiki includes links to several blog posts and documentation about
specific systemd features from an administrators, users or developer perspective, most
prominently featured, the blog post series on Lennart Poettering's blog Pid Eins [^pid-eins].

While these materials provide a good amount of resources for anyone interested in systemd
I found them lacking a single document combining introductory material, architecture
description, background information and actionable descriptions of systemd tools in a single
source presented in a cohesive and easy to follow manner, which I hope to deliver in this
document.

!INCLUDE "introduction.mdpp"
!INCLUDE "manually-starting-a-simple-application.mdpp"
!INCLUDE "automatically-starting-a-simple-service-as-part-of-a-target.mdpp"
!INCLUDE "automatically-starting-a-simple-service-using-socket-activation.mdpp"
!INCLUDE "sd-event-and-sd-bus.mdpp"
!INCLUDE "automatically-starting-a-service-using-dbus-activation.mdpp"
!INCLUDE "interacting-with-systemd-using-the-dbus-api.mdpp"
!INCLUDE "sd-bus-client-example.mdpp"
!INCLUDE "logging-in-systemd.mdpp"
!INCLUDE "journal-message-catalog.mdpp"

# Appendix

!INCLUDE "using-systemd-with-cmake.mdpp", 1
!INCLUDE "references.mdpp", 1
